<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
<h2>Media Online</h2>
<h4>Sosial Media Developer</h4>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<p><b>Benefit Join di Media Online</b></p>
<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<p><b>Cara Bergabung ke Media Online</b></p>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
</body>
</html>