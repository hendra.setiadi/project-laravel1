<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>

<body>
    <h1>Buat Account Baru !</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <p>First Name :</p>
        <input type="text" name='firstName' />
        <p>Last Name :</p>
        <input type="text" name='lastName' />
        <p>Gender :</p>
            <input type="radio" name="gender" ><label for="male">Male</label></br>
            <input type="radio" name="gender" ><label for="female">Female</label></br>
            <input type="radio" name="gender" ><label for="other">Other</label></br>
        <p>Nationality</p>
        <select name="language" >
            <option value="ina">Indonesian</option>
            <option value="uk">English</option>
            <option value="usa">America</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" name="bahasa"/><label for="bahasa">Bahasa Indonesia</label></br>
        <input type="checkbox" name="english"/><label for="english">English</label></br>
        <input type="checkbox" name="other"/><label for="other">Other</label></br>
        <p>Bio:</p>
        <textarea name="bio" rows="10" cols="20"></textarea>    
        <p><input type="submit" value="Sign Up"/></p>
    </form>
</body>

</html>